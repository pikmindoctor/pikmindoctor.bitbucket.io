/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrAI {
    constructor(model) {
        this.model = model;
        this.easyMode = false;
    }

    makeMove() {
        const m = this.model;
        showCursor("wait");
        doLater(() => {
            const canMove = m.rollDie();
            doLater(() => {
                if (canMove) {
                    println("AI Player can move. " + m.dieResult);
                    if (m.piecesThatCanMove != null && m.piecesThatCanMove.length > 0) {
                        const counter = this.findBestMove();
                        const cell = counter.cell;
                        let xCellNumber;
                        let yCellNumber;
                        if (cell == null) {
                            yCellNumber = m.currentPlayer.unusedCountersYCellNumber;
                        } else {
                            xCellNumber = counter.cell.xCellNumber;
                            yCellNumber = counter.cell.yCellNumber;
                        }
                        m.waitingForInput = UrInputToWaitFor.MOVE_CHOICE;
                        m.cellPicked({
                            xCellNumber,
                            yCellNumber
                        });
                    }
                } else {
                    println("AI Player can't move. Die result " + m.dieResult);
                    m.nextPlayer();
                    m.waitForRollDiceCommand();
                }
                showCursor("auto");
            }, 1000);
        }, 250);
    }

    findBestMove() {
        const m = this.model;
        let piece;

        if (this.easyMode) {
            return m.piecesThatCanMove[0];
        }

        //Can a piece take?
        println("Can a piece take?");
        let pieceToMove = null;
        let nextCell;
        let pieceToTake = null;
        for (let a = 0; a < m.piecesThatCanMove.length; a++) {
            piece = m.piecesThatCanMove[a];
            if (piece.cell != null) {
                nextCell = piece.spacesAhead(m.dieResult);
                if (nextCell.counter != null && nextCell.counter.player !== piece.player
                    && (pieceToTake == null || pieceToTake.cell != null && pieceToTake.cell.pathOrder < nextCell.pathOrder)) {
                    pieceToTake = nextCell.counter;
                    pieceToMove = piece;
                }
            }
        }
        if (pieceToMove != null) {
            println("Piece can take");
            return pieceToMove;
        }

        //Move a piece onto the home row
        for (let a = 0; a < m.piecesThatCanMove.length; a++) {
            piece = m.piecesThatCanMove[a];
            if (piece.cell == null) {
                println("Move a piece onto the home row");
                return piece;
            }
        }

        //Can a piece score?
        println("Can a piece score?");
        for (let a = 0; a < m.piecesThatCanMove.length; a++) {
            pieceToMove = m.piecesThatCanMove[a];
            if (pieceToMove.cell != null
                && pieceToMove.spacesAhead(m.dieResult).type === UrCellType.FINAL_FINISH) {
                println("Can score");
                return pieceToMove;
            }
        }
        //Can a piece get to a safe square?
        for (let a = 0; a < m.piecesThatCanMove.length; a++) {
            piece = m.piecesThatCanMove[a];
            if (piece.cell != null) {
                nextCell = piece.spacesAhead(m.dieResult);
                if (nextCell.safe) {
                    println("Piece can move to safety");
                    return piece;
                }
            }
        }
        //Get furthest piece that is not on home row
        let furthestPathOrder = 0;
        let furthestPiece = null;
        for (let a = 0; a < m.piecesThatCanMove.length; a++) {
            piece = m.piecesThatCanMove[a];
            if (piece.cell != null && piece.cell.pathOrder > furthestPathOrder && piece.cell.pathOrder > 4) {
                furthestPathOrder = piece.cell.pathOrder;
                furthestPiece = piece;
            }
        }
        if (furthestPiece != null) {
            println("Move furthest piece that is not on home row");
            return furthestPiece;
        }
        //Use some other piece
        println("Move another piece");
        return m.piecesThatCanMove[0];
    }

}