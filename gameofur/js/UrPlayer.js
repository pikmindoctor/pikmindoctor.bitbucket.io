"use strict";

/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrPlayer {
    constructor(playerConfig) {
        const {name, homeYCellNumber, model, fillStyle, selectedStrokeStyle, deselectedStrokeStyle} = playerConfig;
        this.name = name;
        this.homeYCellNumber = homeYCellNumber;
        this.fillStyle = fillStyle;
        this.selectedStrokeStyle = selectedStrokeStyle;
        this.deselectedStrokeStyle = deselectedStrokeStyle;
        this.model = model;
        this.human = true;
        this.nextPlayerCounterNumber = 0;
        if (this.homeYCellNumber === 0) {
            this.unusedCountersYCellNumber = this.homeYCellNumber - 1;
        } else {
            this.unusedCountersYCellNumber = this.homeYCellNumber + 1;
        }
        this.homeCells = [];
        this.finishingCells = [];
        this.countersInPlay = [];
        this.fullPath = [];
        this.unusedCounters = [];
        this.points = 0;
    }

    counterTaken(counter) {
        counter.cell.counter = null;
        counter.cell = null;
        this.countersInPlay.splice(this.countersInPlay.indexOf(counter), 1);
        this.unusedCounters.push(counter);
        this.model.updateUnusedCounters();
    }

    score() {
        this.points++;
    }

    markPiecesThatCanMove(dieResult) {
        const result = [];
        if (dieResult > 0) {
            this.getPiecesInPlayThatCanMove(dieResult).forEach(piece => {
                piece.movable = true;
                result.push(piece);
            });
        }
        return result;
    }

    turnOver() {
        println("Turn over");
        this.countersInPlay.forEach(piece => {
            piece.movable = false;
        });
        this.unusedCounters.forEach(piece => {
            piece.movable = false;
        });
    }

    getNumberOfCountersOnBoard() {
        return this.countersInPlay == null ? 0 : this.countersInPlay.length;
    }

    counterScored(counter) {
        console.log("Counter scored");
        counter.cell.counter = null;
        counter.cell = null;
        this.countersInPlay.splice(this.countersInPlay.indexOf(counter), 1);
    }

    getUnusedPiecesThatCanMove(dieResult) {
        if (dieResult <= this.model.nHomeCells
            && this.model.spaceOnHomeRowAvailable()) {
            return [...this.unusedCounters];
        } else {
            return [];
        }
    }

    getPiecesInPlayThatCanMove(dieResult) {
        let result = [];
        if (this.model.currentPlayer === this) {
            let counter;
            let nextSpaceNumber;
            for (let a = 0; a < this.countersInPlay.length; a++) {
                counter = this.countersInPlay[a];
                nextSpaceNumber = counter.nextSpaceNumber(dieResult);
                if (nextSpaceNumber < this.model.pathLength) {
                    const nextCell = this.model.getCellByPlayerAndPathOrder(this.model.currentPlayer, nextSpaceNumber + 1);
                    if (nextCell == null) {
                        console.warn("Next cell is null");
                    } else {
                        if (nextCell.counter == null ||
                            !nextCell.safe && nextCell.counter.player !== counter.player) {
                            console.log("Next space number = " + nextSpaceNumber);
                            result.push(counter);
                        } else {
                            if (nextCell.safe) {
                                console.log("Next cell is safe so cannot take")
                            } else {
                                console.log("Next cell is occupied by same player")
                            }
                        }
                    }
                }
            }
            if (dieResult <= this.model.nHomeCells
                && this.model.spaceOnHomeRowAvailable(dieResult)) {
                result = [...result, ...this.unusedCounters];
            }
        }
        return result;
    }


    makeUnusedCounter() {
        const counter = new UrCounter(this, null);
        counter.playerCounterNumber = this.nextPlayerCounterNumber;
        this.nextPlayerCounterNumber++;
        this.unusedCounters.push(counter);
        return counter;
    }

    placeUnusedCounterOnBoard(dieResult) {
        const counter = this.unusedCounters.pop();
        counter.place(this.homeCells[dieResult - 1]);
        this.countersInPlay.push(counter);
        return counter;
    }

    getCounter(cellNumbers) {
        let n = this.fullPath.length;
        let cell;
        for (let pathStep = 0; pathStep < n; pathStep++) {
            cell = this.fullPath[pathStep];
            if (cell.xCellNumber === cellNumbers.xCellNumber
                && cell.yCellNumber === cellNumbers.yCellNumber) {
                return cell.counter;
            }
        }
        return null;
    }
}
