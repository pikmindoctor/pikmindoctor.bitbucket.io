"use strict";

/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
const now = () => (new Date).getTime();
const smootherStep = t => t = t * t * t * (t * (6 * t - 15) + 10);
const rectContains = (point, rect) =>
    point.x >= rect.x
    && point.y >= rect.y
    && point.x <= rect.x + rect.width
    && point.y <= rect.y + rect.height;

const println = text => console.log(text);
let doingSomething = false;
let doLater;
let waitUntilNotDoingSomething;

const showCursor = cursorName => {
    document.getElementById(canvasID).style.cursor = cursorName;
};

waitUntilNotDoingSomething = (fn, waitTime) => {
    if (!doingSomething) {
        fn();
    } else {
        setTimeout(() => {
            if (doingSomething) {
                showCursor("wait");
                waitUntilNotDoingSomething(fn, waitTime);
            } else {
                fn();
            }
        }, waitTime);
    }
};
doLater = (fn, waitTime) => {
    console.log("Start doLater");
    setTimeout(() => {
        waitUntilNotDoingSomething(() => {
            fn();
            doingSomething = false;
            console.log("Doing something is now over");
        }, waitTime)
    }, waitTime);
}