"use strict";

/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrCanvasLocationManager {
    constructor(canvasID) {
        this.view = null;
        this.canvas = document.getElementById(canvasID);
        this.updateSize();
        window.onresize = () => {
            this.updateSize();
            this.view.updateSize();
        }
    }

    updateSize() {
        let dimension = [document.documentElement.clientWidth, document.documentElement.clientHeight];
        this.canvas.width = dimension[0];
        this.canvas.height = dimension[1];
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.centreX = this.width / 2;
        this.centreY = this.height / 2;
        this.cellsHorizontal = 8;
        this.cellsVertical = 3;
        this.margin = Math.min(this.width, this.height) / 3;
        this.boardHeight = Math.min(this.height - this.margin * 2, this.width - this.margin * 2);
        this.cellSize = this.boardHeight / this.cellsVertical;
        this.halfCellSize = this.cellSize / 2;
        this.boardWidth = this.cellsHorizontal * this.cellSize;
        this.boardTop = this.margin;
        this.boardBottom = this.margin + this.boardHeight;
        this.boardLeft = this.centreX - this.boardWidth / 2;
        this.boardRight = this.centreX + this.boardWidth / 2;
        this.middleX = (this.boardLeft + this.boardRight) / 2;
        this.diceRollLeft = this.boardLeft;
        this.diceRollTop = this.height * 0.05;
        if (this.width < this.height) {
            this.diceRollTop += this.height * 0.75;
        }
        this.diceRollWidth = this.width / 10;
    }

    rotated(point) {
        if (this.width < this.height) {
            return {x: point.y, y: point.x};
        } else {
            return point;
        }
    }

    cellPosition(xCellNumber, yCellNumber) {
        let result =
            {
                x: this.boardLeft + this.cellSize * xCellNumber,
                y: this.boardTop + this.cellSize * yCellNumber
            };
        return this.rotated(result);
    }

    cellMiddlePosition(xCellNumber, yCellNumber) {
        let result =
            {
                x: this.boardLeft + this.cellSize * xCellNumber + this.halfCellSize,
                y: this.boardTop + this.cellSize * yCellNumber + this.halfCellSize
            };
        return this.rotated(result);
    }

    cellNumbers(clientX, clientY) {
        if (this.width < this.height) {
            const result = {
                yCellNumber: Math.floor((clientX - this.boardTop) / this.cellSize),
                xCellNumber: Math.floor((clientY - this.boardLeft) / this.cellSize)
            };
            return result;
        } else {
            const result =
                {
                    xCellNumber: Math.floor((clientX - this.boardLeft) / this.cellSize),
                    yCellNumber: Math.floor((clientY - this.boardTop) / this.cellSize)
                };
            return result;
        }
    }
}