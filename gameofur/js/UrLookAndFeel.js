/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrLookAndFeel {
    constructor(lm) {
        this.lm = lm;
        this.backgroundImageSrc = "./img/Wood.png";
        this.fontFamily = "Times New Roman";
        this.uiDialogTitleFont = "30px " + this.fontFamily;
        this.uiDialogContentFont = "20px " + this.fontFamily;
        this.uiButtonFont = "20px " + this.fontFamily;
    }

    setView(view) {
        this.view = view;
        this.ctx = view.ctx;
    }

    afterLoading() {
        const bgImg = this.view.images.get(UrLAFUse.BACKGROUND);
        this.backgroundPattern = this.ctx.createPattern(bgImg, 'repeat');
    }

    updateFonts() {
        this.fontName = "Times New Roman";
        this.fontScale = this.lm.width;
        if (this.lm.width < this.lm.height) {
            this.fontScale *= 1.75;
        }
        println(this.lm.width);
        this.mediumFontSize = 0.02 * this.fontScale;
        this.largeFontSize = 0.033 * this.fontScale;
        this.mediumFont = this.mediumFontSize + "px " + this.fontName;
        this.largeFont = this.largeFontSize + "px " + this.fontName;
    }

    messageFont() {
        this.ctx.fillStyle = "white";
        this.ctx.font = this.mediumFont;
        this.ctx.textAlign = "left";
        this.ctx.textBaseline = "bottom"
    }

    diceRollFont() {
        this.ctx.font = this.mediumFont;
        this.ctx.fillStyle = "white";
        this.ctx.textAlign = "left";
        this.ctx.textBaseline = "bottom"
    }

    dieResultFont() {
        this.ctx.font = this.mediumFont;
        this.ctx.fillStyle = "white";
        this.ctx.textAlign = "left";
        this.ctx.textBaseline = "bottom"
    }

    drawBackground() {
        this.ctx.fillStyle = this.backgroundPattern;
        this.ctx.fillRect(0, 0, this.lm.width, this.lm.height);
        // this.ctx.clearRect(0, 0, this.lm.width, this.lm.height);
    }

    drawCell(cellPosition, cell) {
        this.ctx.beginPath();
        this.ctx.rect(cellPosition.x, cellPosition.y, this.lm.cellSize, this.lm.cellSize);
        this.ctx.strokeStyle = "#D3A07E";
        this.ctx.lineWidth = 3;
        this.ctx.fillStyle = "#F7DEC0";
        this.ctx.fill();
        this.ctx.stroke();
        this.ctx.fillStyle = this.ctx.strokeStyle;
        this.ctx.font = this.mediumFont;
        this.ctx.textAlign = "center";
        this.ctx.textBaseline = "middle";
        this.ctx.fillText(cell.pathOrder, cellPosition.x + this.lm.cellSize / 2, cellPosition.y + this.lm.cellSize / 2);
    }

    drawCounter(counterDrawProperties) {
        this.ctx.beginPath();
        this.ctx.arc(counterDrawProperties.x, counterDrawProperties.y, this.counterSize, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = counterDrawProperties.counter.player.fillStyle;
        this.ctx.fill();
        this.ctx.lineWidth = 4;
        if (counterDrawProperties.counter.movable) {
            this.ctx.strokeStyle = counterDrawProperties.counter.player.selectedStrokeStyle;
        } else {
            this.ctx.strokeStyle = counterDrawProperties.counter.player.deselectedStrokeStyle;
        }
        this.ctx.stroke();
        this.ctx.closePath();
        this.ctx.lineWidth = 1;
    }
}