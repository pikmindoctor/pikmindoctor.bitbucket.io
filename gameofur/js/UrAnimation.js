"use strict";

/**
 * @author Lawrence Watkiss-Veal
 * lwatkissveal@gmail.com
 * 2020
 */
class UrAnimation {
    constructor(props) {
        const {counter, positions, view} = props;
        this.counter = counter;
        this.positions = positions;
        this.startPosition = positions[0];
        this.endPosition = positions[positions.length - 1];
        this.transitionLength = 1000.0;
        this.view = view;
    }

    play() {
        this.startTime = now();
        this.endTime = this.startTime + this.transitionLength;
        this.counter.moving = true;
        window.requestAnimationFrame(() => this.draw());
    }

    draw() {
        let currentTime = now();
        const t = smootherStep((currentTime - this.startTime) / this.transitionLength);
        if ((currentTime = now()) < this.endTime) {
            const index = Math.floor(t * (this.positions.length - 1));
            console.log("Index = " + index);
            const localT = t * (this.positions.length - 1) - index;
            this.positionA = this.positions[index];
            this.positionB = this.positions[index + 1];
            this.vector =
                {
                    x: this.positionB.x - this.positionA.x,
                    y: this.positionB.y - this.positionA.y
                };
            showCursor("wait");
            this.view.drawBoard();
            const counterDrawProperties = {
                x: this.positionA.x + this.vector.x * localT,
                y: this.positionA.y + this.vector.y * localT
            };
            counterDrawProperties.counter = this.counter;
            this.view.drawCounter(counterDrawProperties);
            this.view.drawUI();
            window.requestAnimationFrame(() => this.draw());
        } else {
            if (this.counter.moving) {
                showCursor("auto");
            }
            this.counter.moving = false;
            this.view.animationFinished();
        }
    }
}
